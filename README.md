# Git Tips

There some tips for using Git that makes your repository pretty.

### Commit message

The conventions can be found at [this post](https://chris.beams.io/posts/git-commit/):

> 1. Separate subject from body with a blank line
> 2. Limit the subject line to 50 characters
> 3. Capitalize the subject line
> 4. Do not end the subject line with a period
> 5. Use the imperative mood in the subject line
> 6. Wrap the body at 72 characters
> 7. Use the body to explain what and why vs. how

Check this post for more details and some examples.

### "Linear" Git history

I personally prefer a "linear" Git history. A "linear" Git history makes the commits and branches more readable. Check [this post](http://www.bitsnbites.eu/a-tidy-linear-git-history/) for more details. Basically, use `git rebase [-i]` instead of/before `git merge --no-ff`.
 
### Structure your repository with GitFlow

I find [GitFlow](https://datasift.github.io/gitflow/IntroducingGitFlow.html) provides a good branch structure for Git repositories. The main idea is maintaining two branches `master` and `develop` for releasing code and developing code, respectively. `develop` is for implementing and debugging and when you feel ready, merge `develop` into `master` to keep track of a workable version of the code.

If you want to try new features but are not sure if they are going to work, you can create `feature-` or `features/` branches from `develop` and do experiments there, then throw them away or merge them into `develop` if the new features are beneficial.

There are also branches  like `hotfix` and read the GitFlow post for more information.

### Use of tags

Use [`git tag`](https://git-scm.com/book/en/v2/Git-Basics-Tagging) to mark a releasing version of your code.
